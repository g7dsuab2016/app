﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace G7
{
    public class PainelMostrar : MonoBehaviour
    {
        public static void Mostradanos(float ndanos)
        {
            GameObject.Find("danos").GetComponent<Text>().text = "Danos " + ndanos;
        }

        public static void Mostranomevoltas(string nome,int totalvoltas)
        {
            GameObject.Find("AntesInicio").GetComponent<Text>().text = nome + ", Tem que dar " + totalvoltas + " Voltas";
        }

        public static void MostraMensagem(string msg)
        {
            GameObject.Find("PainelAvisoVolta").GetComponent<Text>().text = msg;
        }

        public static void MostraNvoltas(int nvoltas)
        {
            GameObject.Find("contavoltas").GetComponent<Text>().text = "Nº voltas = " + nvoltas;
        }

        

    }
}