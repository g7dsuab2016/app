﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace G7
{

    public class DanosBarreiraCarro : MonoBehaviour

    {
        // Cria o objeto da interface idano
        IDano DanoCarro1 = new Dano();

        void OnTriggerEnter(Collider other)
        {
            try
            {
                DanoCarro1.Danos(100f);
                // passa para a view a função de mostrar o dado ao utilizador
                PainelMostrar.Mostradanos(DanoCarro1.MostraDanos());                                
            }
            catch
            {
                throw new errointernopassameta();
            }
        }
    }
}
