﻿using System;
using System.Runtime.Serialization;

namespace G7
{
    [Serializable]
    internal class Erronome : Exception
    {
        public Erronome()
        {
        }

        public Erronome(string message) : base(message)
        {
        }

        public Erronome(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected Erronome(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}