﻿using System;
using System.Runtime.Serialization;

namespace G7
{
    [Serializable]
    internal class erroguardanome : Exception
    {
        public erroguardanome()
        {
        }

        public erroguardanome(string message) : base(message)
        {
        }

        public erroguardanome(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected erroguardanome(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}