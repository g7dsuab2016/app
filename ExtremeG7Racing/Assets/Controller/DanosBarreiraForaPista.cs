﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace G7
{

    public class DanosBarreiraForaPista : MonoBehaviour

    {
        // Cria o objeto da interface idano
        IDano DanoFora1 = new Dano();

        void OnTriggerEnter(Collider other)
        {
            try
            {
                DanoFora1.Danos(50f);
                // passa para a view a função de mostrar o dado ao utilizador
                PainelMostrar.Mostradanos(DanoFora1.MostraDanos());
            }
            catch
            {
                throw new errointernopassameta();
            }
        }
    }
}
