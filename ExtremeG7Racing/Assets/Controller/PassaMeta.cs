﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace G7
{
    public class PassaMeta : MonoBehaviour
    {      
        void OnTriggerExit(Collider other)
        {
            try
            {
                {
                    PainelMostrar.Mostradanos(ContaVoltas.health);
                    if (ContaVoltas.flag <= 0)
                    {
                        ++ContaVoltas.nvoltas;
                        // Passa para a VIEW a função de mostrar no ecran
                        PainelMostrar.MostraMensagem(Jogador.nome + ", tem que passar pela meta do outro lado da pista!");
                        PainelMostrar.MostraNvoltas(ContaVoltas.nvoltas);
                        ContaVoltas.flag = 1;                
                    }
                }
            }
            catch {
                throw new errointernopassameta();
            }
        }
    }
}

