﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace G7
{
    public class Timer : MonoBehaviour {
        public static float sec, min, frac;
        public float secantes, minantes,fracantes;
        public string texttime;
        // Use this for initialization
        void Start () {
            ContaVoltas.flag = -1;
        }
	    // Update is called once per frame
        void Update () {
            if (ContaVoltas.flag >= 0)
            {
                min = (int)(Time.time / 60f)  ;
                sec = (int)(Time.time % 60f) - minantes ;
                frac = (int)(Time.time * 100) % 100;
                texttime = string.Format("{0:00}:{1:00}:{2:00}",min,sec,frac);
                GameObject.Find("Tempo").GetComponent<Text>().text = "Tempo = " + texttime ;
            } else {
                secantes= (int)(Time.time / 60f);
                minantes= (int)(Time.time % 60f);
                fracantes = (int)(Time.time * 100) % 100;
            }
        }
    }

}