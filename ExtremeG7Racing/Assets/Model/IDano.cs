﻿using UnityEngine;
using System.Collections;
using System;

namespace G7
{
    interface IDano
    {
        void Danos(float valordanos);
        float MostraDanos();
    }
}