﻿using UnityEngine;
using System.Collections;

namespace G7
{
	public class Dano : MonoBehaviour, IDano
	{
		

		public float MostraDanos  ()
		{
			return(ContaVoltas.health);			
		}

		public void Danos (float valordanos)
		{
            ContaVoltas.health -= valordanos;
		}
	}

	
}